package br.com.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class CalculadoraTeste {

    @Test
    public void testarSomaDeDoisNumeros() {
        int resultado = Calculadora.soma(2,2);

        Assertions.assertEquals(4, resultado);
    }
    @Test
    public void testarDivisaoDeDoisNumeros() {
        int resultado = Calculadora.divide(2,2);

        Assertions.assertEquals(1, resultado);
    }
//    @Test //(expected = ArithmeticException.class)
//    public void testarDivisaoPorZero(){
//        final int resultado = Calculadora.divide(2,0);
//
//        Assertions.assertThrows(ArithmeticException.class, () -> );
//    }
    @Test
    public void testarMultiplicacaoDeDoisNumeros() {
        int resultado = Calculadora.multiplica(3,2);

        Assertions.assertEquals(6, resultado);
    }
    @Test
    public void testarSubtracaoDeDoisNumeros() {
        int resultado = Calculadora.subtrai(2,2);

        Assertions.assertEquals(0, resultado);
    }
}
