package br.com.tdd;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class ContaTeste {

    private Cliente cliente;
    private Conta conta;

    @BeforeEach
    private void setUp(){
        this.cliente=new Cliente("Mauricio");
        this.conta=new Conta(cliente, 300.00);
    }

    @Test
    public void testarDepositoEmConta() {

        double valorDeposito = 100.00;
        conta.depositar(valorDeposito);

        Assertions.assertEquals(400.00, conta.getSaldo());
    }

    @Test
    public void testarSaqueSemSaldo() {

        final double valorDeSaque = 1000.00;

        Assertions.assertThrows(RuntimeException.class, () -> {conta.sacar(valorDeSaque);});
    }
}
