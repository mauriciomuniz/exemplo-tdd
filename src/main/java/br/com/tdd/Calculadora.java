package br.com.tdd;

public class Calculadora {
    public static int soma(int primeiroNumero, int segundoNumero) {
        int resultado=primeiroNumero+segundoNumero;

        return resultado;
    }
    public static int divide(int primeiroNumero, int segundoNumero) {
        int resultado=0;
        try {
            resultado = primeiroNumero / segundoNumero;
            return resultado;
        } catch (ArithmeticException e) {
            throw new ArithmeticException("Divisao por zero");
        }
    }
    public static int multiplica(int primeiroNumero, int segundoNumero) {
        int resultado=primeiroNumero*segundoNumero;

        return resultado;
    }
    public static int subtrai(int primeiroNumero, int segundoNumero) {
        int resultado=primeiroNumero-segundoNumero;

        return resultado;
    }
}
