package br.com.tdd;

public class Conta {

    private Cliente cliente;
    private double saldo;

    public Conta(Cliente cliente, double saldo) {
        this.cliente = cliente;
        this.saldo = saldo;
    }

    public double depositar(double valorDeposito) {
        return this.saldo += valorDeposito;
    }

    public double sacar(double valorDeSaque) {
        if (valorDeSaque > this.saldo) {
            throw new RuntimeException("Ssaldo Insuficiente");
        } else {
            return this.saldo -= valorDeSaque;
        }
    }

    public Cliente getCliente() {
        return cliente;
    }

    public void setCliente(Cliente cliente) {
        this.cliente = cliente;
    }

    public double getSaldo() {
        return saldo;
    }

    public void setSaldo(double saldo) {
        this.saldo = saldo;
    }
}
